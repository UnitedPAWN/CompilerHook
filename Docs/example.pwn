
#define FILTERSCRIPT
#include <a_samp>


#tryinclude "gitversion.pwn"
#if !defined GIT_SHA1_LONG
new const stock GIT_SHA1_LONG[] = "Unknown";
new const stock GIT_SHA1_SHORT[] = "Unknown";
#endif



public OnFilterScriptInit() {
    printf(	  	"\n"                                                \
				"   +------------------------------------+\n" 		\
				"   |  ***    Git Example Script    ***  |\n" 		\
				"   +------------------------------------+\n" 		\
				"\n"												\
 				"   +------------------------------------+\n"     	\
				"   |  Autor:                 xhunterx   |\n"    	\
				"   |  SA-MP Version 0.3.7 R2 Linux/Win  |\n"     	\
				"   |  Git SHA1 Version:      %s    |\n"          	\
				"   +------------------------------------+\n\n", GIT_SHA1_SHORT);
	return 1;
}



