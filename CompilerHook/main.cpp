/*
	Copyright(C) 2017  xhunterx

	This program is free software : you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <fstream>

std::string exec(const char* cmd) {
	char buffer[128];
	std::string result = "";
	std::shared_ptr<FILE> pipe(_popen(cmd, "r"), _pclose);
	if (!pipe) throw std::runtime_error("popen() failed!");
	while (!feof(pipe.get())) {
		if (fgets(buffer, 128, pipe.get()) != NULL)
			result += buffer;
	}
	return result;
}

std::string escape_spaces(std::string str) {
	size_t pos = 0;
	while ((pos = str.find(' ', pos)) != str.npos) {
		str.insert(pos, "^");
		pos += 2;
	}
	return str;
}

int main(int argc, char ** argv) {
	bool uncommited = false;
	unsigned int warnings = 0, errors = 0;

	std::string str = "\"\"";
	str += argv[0];

	size_t pos = 0;
	/*while ((pos = str.find(' ', pos + 2)) != str.npos) {
		str.insert(pos, "^");
	}*/
	pos = str.find(".exe");
	if(pos != str.npos) str.insert(pos, "2");
	else str += "2";

	str += "\"";

	for (int i = 1; i < argc; ++i) {
		str += " ";
		if (i == 3) str += "\"";
		str += argv[i];
		if (i == 3) str += "\"";
	}
	str += "\"";

	std::string filename;
	std::string SHA1 = exec("git rev-parse HEAD 2>&1");
	
	if (SHA1.length() > 67) {
		std::cout << argv[3] << " : git error 1: Source is not in git repository." << std::endl;
		++errors;
	}
	else if (SHA1.size() > 2) {
		std::string ret = exec("git diff-index --quiet HEAD -- || echo Uncommited changes.");
		if (ret.length() > 10) {
			std::cout << argv[3] << " : git warning 2: Uncommited changes." << std::endl;
			uncommited = true;
			++warnings;
		}
		filename = argv[argc-1];

		while ((pos = filename.find('\\', pos + 1)) != filename.npos) {
			filename[pos] = '/';
		}

		filename = filename.substr(0, filename.rfind("/"));
		filename += "/gitversion.pwn";

		std::fstream file;
		file.open(filename, std::fstream::out);

		file << "new const stock GIT_SHA1_LONG[] = \"";
		file << SHA1.substr(0, SHA1.length() - 1);
		if (uncommited) file << "-dev";
		file << "\";" << std::endl;

		file << "new const stock GIT_SHA1_SHORT[] = \"";
		file << SHA1.substr(0, 7);
		if (uncommited) file << "-dev";
		file << "\";" << std::endl;

		file.close();
	}
	else std::cout << std::endl;
	
	std::string summary = exec(str.c_str());
	size_t warPos = summary.find("");
	std::cout << summary;

	if (errors == 1)		std::cout << errors << " Git Error."  << std::endl;
	else if(errors)			std::cout << errors << " Git Errors." << std::endl;
	else if (warnings == 1) std::cout << warnings << " Git Warning." << std::endl;
	else if (warnings)		std::cout << warnings << " Git Warnings." << std::endl;

	if(filename.length()) std::remove(filename.c_str());
	return 0;
}